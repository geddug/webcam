<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('Cam');
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($_FILES != NULL) {
			$this->Cam->add();
		} else {
			$this->load->view('home');
		}
	}
	public function data() {
		header('Content-Type: application/json');
        $data = $this->Cam->json();
		print_r($data);
	}
	public function edit($id) {
		$a =  $this->Cam->edit($id);
		echo $a->video;
	}
	public function hapus($id) {
		$this->Cam->delete($id);
		echo json_encode(array("status" => TRUE));
	}
}
