<?php
class Cam extends CI_model {
  public function json() {
		$this->datatables->select('id,video,date');
        $this->datatables->from('record');
		$this->datatables->add_column('action', '<a href="#" onclick="ganti($1)" class="btn btn-primary">View</i></a> 
		<a href="#" onclick="hapus($1)" class="btn btn-danger">Delete</a>', 'id');
        return $this->datatables->generate();
  }
  public function add() {
		$config['upload_path']   = './video/';
		$config['file_name'] = time().".webm";
	    $config['allowed_types'] = '*';
	    $config['max_size']      = 0;
	    $this->load->library('upload', $config);
	    
	    $this->upload->do_upload('data');
	    $data = $this->upload->data();
		$size = $data['file_size'];
		if ($size != NULL) {
			$video = $data['file_name'];
		} else {
			$video = NULL;
		}
		
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d H:i:s');
		$datalog = array('video' => $video, 'date' => $date);
		$this->db->insert('record', $datalog);
		$this->db->insert_id();
	}
	public function edit($id) {
		$this->db->where('id', $id);
		return $this->db->get('record')->row();
	}
	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('record');
	}
}