
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Webcam Record Video</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/assets/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="assets/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.min.css" rel="stylesheet">
	<link href="assets/css/addons/datatables.css" rel="stylesheet">
</head>

<body>

    <!-- Start your project here-->
    <div class="container">
		<div class="row mt-3">
			<div class="col-md-6">
				<video id="preview" class="border border-primary" style="width:100%" autoplay muted></video>
			</div>
			<div class="col-md-6 text-center">
				<video id="recording" style="width:100%" controls class="border border-primary"></video>
			</div>
			<div class="col-md-12 text-center">
				<button class="btn btn-primary" onclick="start()">Start</button>
				<button class="btn btn-danger" onclick="pause()">Stop</button>
			</div>
			<div class="bottom col-md-12">
			  <pre id="log"></pre>
			</div>
			<div class="col-md-12">
				<div class="card-box">
                  <table id="datatable" class="table table-striped table-bordered">
                     <thead>
                        <tr>
                           <th>Video</th>
                           <th>Date</th>
						   <th>act</th>
                        </tr>
                     </thead>
                     <tbody>
                     </tbody>
                  </table>
               </div>
			</div>
		</div>
	</div>
    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="assets/js/mdb.min.js"></script>
	<script type="text/javascript" src="assets/js/addons/datatables.js"></script>
	<script type="text/javascript" src="assets/js/video.js"></script>
</body>

</html>
