var table;
$(document).ready(function () {
	table = $('#datatable').DataTable({
		"processing": !0,
		"serverSide": !0,
		"order": [],
		"ajax": {
			"url": 'data',
			"type": "POST"
		},
		"columns": [{
			"data": "video"
		}, {
			"data": "date"
		},{
			"data": "action"
		}],
	})
});
function hapus(id) {
	$.ajax({
			url: "hapus/" + id,
			type: "POST",
			dataType: "JSON",
			success: function (data) {
				table.ajax.reload()
			}
		});
}
function ganti(id) {
	$("#recording").load("edit/" + id, function (data) {
		$("#recording").attr('src', "video/"+data);
	})
}

var preview = document.getElementById("preview");
var recording = document.getElementById("recording");
//var downloadButton = document.getElementById("downloadButton");
var logElement = document.getElementById("log");

var recordingTimeMS = 5000;
function log(msg) {
  logElement.innerHTML += msg + "\n";
}
function wait(delayInMS) {
  return new Promise(resolve => setTimeout(resolve, delayInMS));
}
function startRecording(stream, lengthInMS) {
  var recorder = new MediaRecorder(stream);
  var data = [];
 
  recorder.ondataavailable = event => data.push(event.data);
  recorder.start();
  log(recorder.state + " for " + (lengthInMS/1000) + " seconds..");
 
  var stopped = new Promise((resolve, reject) => {
    recorder.onstop = resolve;
    recorder.onerror = event => reject(event.name);
  });

  var recorded = wait(lengthInMS).then(
    () => recorder.state == "recording" && recorder.stop()
  );
 
  return Promise.all([
    stopped,
    recorded
  ])
  .then(() => data);
}
function stop(stream) {
  stream.getTracks().forEach(track => track.stop());
}
function start() {
  navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true
  }).then(stream => {
    preview.srcObject = stream;
    //downloadButton.href = stream;
    preview.captureStream = preview.captureStream || preview.mozCaptureStream;
    return new Promise(resolve => preview.onplaying = resolve);
  }).then(() => startRecording(preview.captureStream(), recordingTimeMS))
  .then (recordedChunks => {
    var recordedBlob = new Blob(recordedChunks, { type: "video/webm" });
	//saveAs(recordedBlob, "rec.webm");
    recording.src = URL.createObjectURL(recordedBlob);
    //downloadButton.href = recording.src;
    //downloadButton.download = "RecordedVideo.webm";
    post(recordedBlob);
    log("Success " + recordedBlob.size + " bytes of " +
        recordedBlob.type + " media.");
  })
  .catch(log);
}
function pause() {
  stop(preview.srcObject);
}
function post(val) {
	var fd = new FormData();
	fd.append('data', val);
	$.ajax({
		type: 'POST',
		url: '',
		data: fd,
		processData: false,
		contentType: false,
		success: function (data) {
			table.ajax.reload();
		}
	})
}